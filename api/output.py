import psycopg2
connection = psycopg2.connect(host='localhost', database='postgres', user='purushottam.gour', password='hotelsoft')
cursor=connection.cursor()
option=None


# print reservation along with columns
def printreservations(result, *args):
    # print headers:
        print('#' * (len(args)*20))
        fmtstring = '{:20}'*len(args)
        print(fmtstring.format(*args))
        print('-' * (len(args)*20))
        #print Data:
        for reservation in result:
            for arg in range(len(args)):
                if reservation[arg] is not None:
                    print('{:<20}'.format(reservation[arg]), end='')
                else:
                    print('{:<20}'.format('!Not Specified'), end='')
            print('')
        print('#' * (len(args)*20))
        print('')



#Top n Countries where reservations came from and the number of reservations and room nights associated with each
def top_Cities_for_reservation():
    cursor.execute('SELECT gstcountrycode,count(reservationid) as totalreservations,sum(nights),sum(totalrevenue) FROM reservation GROUP BY gstcountrycode  ORDER BY sum(totalrevenue) DESC limit 10')
    result = cursor.fetchall()
    printreservations(result,'GSTCOUNTRYCODE','RESERVATIONS','NIGHTS','TOTAL REVENUE')
    country=input("Type country to get top cities or 0 to exit: ")
    if country != '0':
        topCities(country)


#Top N block reservations and the number of room nights booked
def block_reservations():
    cursor.execute('SELECT blockid,count(*),sum(totalrevenue),sum(nights) FROM reservation where blockid!=0 GROUP BY blockid ORDER BY sum(nights) DESC LIMIT 10')
    result=cursor.fetchall()
#Top n Countries/Cities/zip-codes where reservations came from and the number of reservations and room nights associated with each

    printreservations(result,'BlOCKID','COUNT','TOTAL REVENUE','NIGHTS')



#List all reservations that are not valid(Room type not in valid list or Rate Category not in valid list)
def Valid_reservation():
    cursor.execute("select reservationid,roomtype,ratecategory from reservation where ratecategory NOT IN ('Rack/General','Promotional','Family','Government','Military','Senior Citizen','Tour','Convention','Corporate','Association','Travel Industry','Weekend Negotiated','Group') or roomtype NOT IN('KN','DN','KND','DND','KNS','JN','JSD','PRES','DNAD','KS','DS','JNAC','DNA','KNA','KNAD') limit 20")
    result=cursor.fetchall()
    printreservations(result,'RESERVATIONS ID','ROOMTYPE','RATE CATEGORY')




#Top 10 non block customers(where the sum of revenue + package revenue is the maximum
def top_non_block_customers():
    cursor.execute("SELECT firstname,lastname,sum(totalrevenue+packagerevenue) FROM reservation where blockid =0 group by 1,2 order by 3 desc limit 10")
    result=cursor.fetchall()
    printreservations(result,'FIRST NAME','LAST NAME','TOTOAL REVENUE')




#Top N Companies
def Top_companies():
    cursor.execute("select companyname,sum(totalrevenue) from reservation group by companyname order by companyname desc limit 20")
    result=cursor.fetchall()
    printreservations(result,'COMPANY NMAE','TOTAL REVENUE')




#Count All reservations that are in a given rate category. Give both count, sum(revenue) per ecah rate category
def reservation_for_ratecategory():
    cursor.execute("select ratecategory, count(*), sum(totalrevenue) from reservation where ratecategory in ('Rack/General', 'Promotional','Family','Government','Military','Senior Citizen','Tour','Convention','Corporate','Association','Travel Industry','Weekend','Negotiated','Group') group by ratecategory")
    result=cursor.fetchall()
    printreservations(result,'RATECATEGORY','COUNT','TOTAL REVENUE')



#Top n Cities where reservations came from and the number of reservations and room nights associated with each
def topCities(country):
    query = f"""SELECT gstcity, COUNT(reservationid), sum(nights),sum(totalrevenue) FROM reservation WHERE gstcountrycode = '{country}' AND gstcity != 'None' GROUP BY gstcity ORDER BY sum(totalrevenue) DESC LIMIT 10 """
    cursor.execute(query)
    result=cursor.fetchall()
    printreservations(result,'CITY','NO OF RESERVATIONS','NIGHTS','REVENUE')	
    city=input("Type city to get top zipcodes or 0 to exit: ")
    if city != '0':
        topZipCodes(city)



#Top n zip-codes where reservations came from and the number of reservations and room nights associated with each
def topZipCodes(city):
    query = f"""SELECT gstzip, COUNT(reservationid), sum(nights),sum(totalrevenue) FROM reservation  WHERE gstcity = '{city}'  AND gstzip != 'None' GROUP BY gstzip ORDER BY sum(totalrevenue) DESC LIMIT 10 """
    cursor.execute(query)
    result=cursor.fetchall()
    printreservations(result,'ZIPCODE','NO OF RESERVATIONS','NIGHTS','REVENUE')	






#Listing Menu's on Dashboard:

while option !=0:
    print("*************************  PLEASE ENTER A VALID OPTION TO QUERY FROM DATABASE ****************************")
    print("0: exit")
    print("1:  Top n Countries/Cities/zip-codes where reservations came from and the number of reservations and room nights associated with each")
    print("2: Top n block reservations and the number of room nights booked")
    print("3:  List all reservations that are not valid (Room type not in valid list or Rate Category not in valid list)")
    print("4: Top 10 non block customers (where the sum of revenue + package revenue is the maximum)")
    print("5: Top n Companies")
    print("6: Count all reservation that are in a given rate category.  Give both count, sum(revenue) per each rate category.")

    option=int(input("enter your choice: "))


    if option==0:
        print("THANK YOU!\n  ******VISIT AGAIN********")
        exit()

    elif option==1:
        top_Cities_for_reservation() 
    
    elif option==2:
        block_reservations()

    elif option==3:
        Valid_reservation()

    elif option==4:
        top_non_block_customers()

    elif option==5:
        Top_companies()

    elif option==6:
        reservation_for_ratecategory()

    else:
        print("Invlid choice! \n *******Choose Valid option!**********")
