#!/usr/bin/env python3
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import csv
from datetime import datetime

reservationdic = {}
app=Flask(__name__)
db=SQLAlchemy()
db.init_app(app)
def formatreservation(reservation):
    #reservation['extractdate'] = datetime.strptime(reservation['extractdate'], '%Y-%m-%d')
    reservation['arrival'] = datetime.strptime(reservation['arrival'], '%m/%d/%Y %I:%M:%S %p')
    reservation['departure'] = datetime.strptime(reservation['departure'], '%m/%d/%Y %I:%M:%S %p')
    reservation['nights'] = int(reservation['nights'])
    reservation['roomrevenue'] = float(reservation['roomrevenue'])
    reservation['totalrevenue'] = reservation['roomrevenue'] * float(reservation['quantity'])
    reservation['gstcountrycode'] = reservation['gstcountrycode'].strip()
    return reservation

#Open the file in readonly mode
with open('reservations.csv', 'r') as file:
    # Create dictionary reader for csv data
    csvreader = csv.DictReader(file)
    for reservation in csvreader:
        # Ignore cancellation
        if reservation['status'] == 'C':
            continue
        # fix data types
        reservation = formatreservation(reservation)
        reservationd = reservation['reservationid']
        # eleminate duplicate reservations based reservation ID
        if reservationd in reservationdic:
            # merge of the new reservation with existing reservation
            existingreservation = reservationdic[reservationd]
            existingreservation['arrival'] = min(existingreservation['arrival'], reservation['arrival'])
            existingreservation['departure'] = max(existingreservation['departure'], reservation['departure'])
            existingreservation['roomrevenue'] = str(existingreservation['roomrevenue']) + '|' + str(reservation['roomrevenue'])
            existingreservation['totalrevenue'] = existingreservation['totalrevenue'] + reservation['totalrevenue']
            reservationdic[reservationd] = existingreservation
        else:
            # new reservation stored in dictionary
            reservationdic[reservationd] = reservation

columns = list(reservationdic.values())[0].keys()
#print(columns)
values = list(reservationdic.values());
with open('reserv.csv', 'w') as writeFile:
  writer = csv.DictWriter(writeFile, fieldnames = columns)
  writer.writeheader()
  writer.writerows(values)



#class for Reservations:
class Reservation(db.Model):
    __tablename__ = 'reservation'
    extractdate = db.Column(db.Date, nullable=True)
    resortid = db.Column(db.Integer, nullable=True)
    reservationid = db.Column(db.Integer, nullable=True, primary_key=True)
    confirmationno = db.Column(db.VARCHAR, nullable=True)
    booktime = db.Column(db.DateTime, nullable=True)
    status = db.Column(db.VARCHAR, nullable=True)
    arrival = db.Column(db.DateTime, nullable=True)
    departure = db.Column(db.DateTime, nullable=True)
    staydate = db.Column(db.DateTime, nullable=True)
    adults = db.Column(db.Integer, nullable=True)
    children = db.Column(db.Integer, nullable=True)
    nights = db.Column(db.Integer, nullable=True)
    ratecode = db.Column(db.VARCHAR, nullable=True)
    ratecodename = db.Column(db.VARCHAR, nullable=True)
    ratecategory = db.Column(db.VARCHAR, nullable=True)
    roomtype = db.Column(db.VARCHAR, nullable=True)
    quantity = db.Column(db.Integer, nullable=True)
    packages = db.Column(db.VARCHAR, nullable=True)
    roomrevenue = db.Column(db.VARCHAR, nullable=True)
    packagerevenue = db.Column(db.Numeric, nullable=True)
    otherrevenue = db.Column(db.Numeric, nullable=True)
    reservationtype = db.Column(db.VARCHAR, nullable=True)
    marketcode = db.Column(db.VARCHAR, nullable=True)
    marketname = db.Column(db.VARCHAR, nullable=True)
    sourcedescription = db.Column(db.VARCHAR, nullable=True)
    origin = db.Column(db.VARCHAR, nullable=True)
    companyid = db.Column(db.Integer, nullable=True)
    companyname = db.Column(db.VARCHAR, nullable=True)
    travelagentid = db.Column(db.Integer, nullable=True)
    blockid = db.Column(db.Integer, nullable=True)
    guestid = db.Column(db.Integer, nullable=True)
    firstname = db.Column(db.VARCHAR, nullable=True)
    lastname = db.Column(db.VARCHAR, nullable=True)
    updatedate = db.Column(db.DateTime, nullable=True)
    routingcode = db.Column(db.VARCHAR, nullable=True)
    modifydate = db.Column(db.DateTime, nullable=True)
    gstaddress = db.Column(db.VARCHAR, nullable=True)
    gstcity = db.Column(db.VARCHAR, nullable=True)
    gstregion = db.Column(db.VARCHAR, nullable=True)
    gstregioncode = db.Column(db.VARCHAR, nullable=True)
    gstzip = db.Column(db.VARCHAR, nullable=True)
    gstcountrycode = db.Column(db.VARCHAR, nullable=True)
    newroomrevenue = db.Column(db.Numeric, nullable=True)
    staydays = db.Column(db.Integer, nullable=True)
    totalrevenue = db.Column(db.Numeric, nullable=True)
    sumofroomrevenue = db.Column(db.Numeric, nullable=True)
    warning = db.Column(db.VARCHAR, nullable=True)

